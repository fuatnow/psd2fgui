import { ExportConfig, E_Option } from "./Common";
import UIPackage from "./UIPackage";


export default class Psd2Fgui
{
    public static convert(argv:string[])
    {
        //console.log(argv)
        if(argv.length < 4)
        {
            console.info('Usage: psd2fgui psdFile outputFile packName subCom. exp: node Psd2Fgui  D:/Project/_psd2fgui/test/test.psd D:/Project/fguiTest/FGUIProject/assets/ Bag Card');
            process.exit(1);
        }

        let option:E_Option = E_Option.DEFAULT;
        for (var i = 0; i < argv.length; i++) 
        {
            var arg = argv[i];
            if (arg.indexOf('--') == 0) {
                switch (arg.substr(2)) {
                    case 'ignore-font':
                        option |= E_Option.IGNORE_FONT;
                        break;
                    default:
                        console.error('unknown argument: ' + arg);
                        break;
                }
            }
        }

        let cfg:ExportConfig = {
            psdFile:argv[0],
            outPath:argv[1],
            mainCom:argv[2],
            subCom:argv[3],
            option:option,
        };
        new UIPackage(cfg).exportPackge().then((pack:UIPackage) => {
            console.log(`Package (${pack.cfg.mainCom})[${pack.cfg.subCom}] export ok. buildId: ${pack.buildId}`)
        });
    }
}

Psd2Fgui.convert(process.argv.slice(2));
