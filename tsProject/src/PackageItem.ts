
import { E_Item_Type } from "./Common";
import Utils from "./Utils";

export default class PackageItem
{
    private _type:E_Item_Type;
    private _id:string;
    private _name:string;
    private _data:any;
    private _scale9Grid:number[] | undefined;
    private _existed:boolean;

    public get id():string
    {
        return this._id;
    }

    public set id(val:string)
    {
        this._id = val;
    }

    public get type():E_Item_Type
    {
        return this._type;
    }

    public get data():any
    {
        return this._data;
    }

    public get width():number
    {
        return this._data.get('width');
    }

    public get height():number
    {
        return this._data.get('height');
    }

    public get name():string
    {
        return this._name;
    }

    public get existed():boolean
    {
        return this._existed;
    }

    public set existed(val:boolean)
    {
        this._existed = val;
    }

    /**
     * 格式化九宫格数据
     */
    public async format9ScaleData():Promise<void>{
        if(!this._scale9Grid) return;

        let channelImg = this.data.get('image');
        let imgBuff:Buffer = Buffer.from(channelImg.pixelData);
        let scale9Width = this._scale9Grid[0] + this._scale9Grid[4] + this._scale9Grid[2];
        let scale9Height = this._scale9Grid[1] + this._scale9Grid[3] + this._scale9Grid[5];
        let newImgUint8:Uint8Array = await Utils.formatScale9Buff(imgBuff,{
            width : this.width,
            height : this.height,
            left : Number(this._scale9Grid[0]),
            up : Number(this._scale9Grid[1]),
            middleWidth : Number(this._scale9Grid[2]),
            middleHeight :Number( this._scale9Grid[3]),
            right : Number(this._scale9Grid[4]),
            bottom : Number(this._scale9Grid[5])
        });
        channelImg.obj._width = scale9Width;
        channelImg.obj._height = scale9Height;
        channelImg.obj.pixelData = newImgUint8;
    }

    /**
     * 设置9宫左上右下
     */
    public set scale9Grid(scale9Grid:number[]|undefined)
    {
        if(scale9Grid && scale9Grid.length > 0)
        {
            this._scale9Grid = scale9Grid;
        }
    }

    public get scale9Grid():number[]|undefined
    {
        return this._scale9Grid;
    }

    public constructor(type:E_Item_Type, fileName:string, data:any,id:string)
    {
        this._type = type;
        this._id = id;
        this._name = fileName;
        this._data = data;
        this._scale9Grid = undefined;
        this._existed = false;
    }
}