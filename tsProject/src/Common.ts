export interface ExportConfig
{
    psdFile:string;                 //psd路径
    outPath:string,                 //输出路径
    mainCom:string,                 //包名
    subCom:string,                  //子组件名称
    option:number                   //导出参数默认是0
}

/**
 * 导出参数
 */
export enum E_Option
{
    DEFAULT = 0,
    IGNORE_FONT = 1,
    NO_PACK = 2,
}

/**
 * 控件前缀
 */
export enum E_Prefix
{
    COMPONENT = 'Com$',
    BUTTON = 'Btn$',
    RADIOBUTTON = 'RadBtn$',
    CHECKBUTTON = 'ChkBtn$',
    PROGRESSBAR = 'Bar$',
}

export type NumArray = number[];

const suffixValArr = ['@title' , '@icon' , '@checked' , '@titleColor'] as const;
/**
 * 控件后缀
 */
export enum E_Suffix
{
    TITLE = '@title',
    ICON = '@icon',
    CHECKED = '@checked',
    BAR = '@bar',
    Grid9 = '@9#',
}

export type ValSuffix = { [T in (typeof suffixValArr)[number]] ?: any; }

/**
 * 按钮状态
 */
export enum E_ButtonStatus_Suffix
{
    Up = '@up',
    Down = '@down',
    CHECKED = '@check',
}

export type KeyButtonStatus = keyof typeof E_ButtonStatus_Suffix;

/**
 * 控件类型
 */
export enum E_Item_Type
{
    IMAGE = 'image',
    COMPONENT = 'component',
}

/**
 * 9宫格配置
 */

 export interface Scale9_Cfg
 {
    width : number,
    height : number;
    left : number,
    up : number,
    middleWidth : number,
    middleHeight : number,
    right : number,
    bottom : number
}