psd2fgui
=============

psd文件转换成fgui文件工具。在[源psd2fgui](https://github.com/fairygui/psd2fgui)基础上改进，使用ts改写，同时扩展了9宫以及进度条功能。

* 项目有两个目录:
    * tsProject是用来生成psd2fgui.js的。
    * DemoFguiProject是写的插件调用psd2fgui.js，省的每次转换都手敲命令行。使用DemoFguiProject\plugins\psd2fgui插件的时候需要执行一下`npm install`安装依赖包。
    
### 安装 ###

```sh
npm install
```

### 编译 ###

```sh
gulp compile
```

### 转换 ###

```sh
node ./build/psd2fgui ./test/test.psd ./test/ Demo Main
```
* 参数意义
    *   参数1： 表示psd路径 
    *   参数2： 表示输出路径 
    *   参数3： 表示包名 
    *   参数4： 表示组件名

* 输出结果包含组件和资源两个独立包,放到fgui工程的assets目录下，刷新一下就可以了。
    
![导出内容](psdrule/img/13.png) 

#### 详见 [psd规范](psdrule/PsdRule.md)