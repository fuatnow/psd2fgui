const rollup = require("rollup");
const typescript = require('rollup-plugin-typescript2');//typescript2 plugin
function compile() 
{
	return rollup.rollup({
        input:  './src/Psd2Fgui.ts',
        onwarn:(waring,warn)=>{
            if(waring.code == "CIRCULAR_DEPENDENCY"){
                console.log("warnning Circular dependency:");
                console.log(waring);
            }
        },
        treeshake: false, //建议忽略
        plugins: [
            typescript({
                tsconfig:"./tsconfig.json",
                check: true, //Set to false to avoid doing any diagnostic checks on the code
                tsconfigOverride:{compilerOptions:{removeComments: false}},
                include:/.*.ts/,
            }),      
        ]
    }).then((bundle) => {
        return bundle.write({
            file:  './build/psd2fgui.js',
            format: 'iife',
            name: 'psd2fgui',
            sourcemap: true
        });
    }).catch((err) => console.log(err))
}

exports.compile = compile;