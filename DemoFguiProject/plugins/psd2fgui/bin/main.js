"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.onDestroy = exports.onEnable = void 0;
const csharp_1 = require("csharp");
const puerts_1 = require("puerts");
const App = csharp_1.FairyEditor.App;
const Path = csharp_1.System.IO.Path;
const CUR_PATH = eval("__dirname");
const Path_Project = CUR_PATH.substr(0, CUR_PATH.indexOf("/"));
const Path_Bin = Path_Project + "\\bin\\";
const ROOT_BIN = csharp_1.FairyEditor.App.pluginManager.basePath + "\\" + Path_Bin;
const FAIRUI = ROOT_BIN + "res\\";
const CACHE_FILEPATH = ROOT_BIN + ".cache";
let cacheMap = loadCache();
function onEnable() {
}
exports.onEnable = onEnable;
function onDestroy() {
}
exports.onDestroy = onDestroy;
function saveCache() {
    if (!csharp_1.System.IO.File.Exists(CACHE_FILEPATH)) {
        let dir = Path.GetDirectoryName(CACHE_FILEPATH);
        if (!csharp_1.System.IO.Directory.Exists(dir)) {
            csharp_1.System.IO.Directory.CreateDirectory(dir);
        }
        let file = csharp_1.System.IO.File.Create(CACHE_FILEPATH);
        file.Close();
        console.log("创建文件: " + CACHE_FILEPATH);
    }
    let json = JSON.stringify(cacheMap);
    try {
        csharp_1.System.IO.File.WriteAllText(CACHE_FILEPATH, json, csharp_1.System.Text.Encoding.UTF8);
    }
    catch (error) {
        console.log("------>写入文件失败 url: " + CACHE_FILEPATH + " json: " + json);
        console.error(error);
    }
}
function loadCache() {
    if (csharp_1.System.IO.File.Exists(CACHE_FILEPATH)) {
        try {
            let txt = csharp_1.System.IO.File.ReadAllText(CACHE_FILEPATH) || "{}";
            let obj = JSON.parse(txt);
            return obj;
        }
        catch (error) {
            console.log("读取缓存失败");
            console.error(error);
        }
    }
    return {};
}
function browsePsdFile(view) {
    let psdPath = cacheMap["psdFile"] ? Path.GetDirectoryName(cacheMap["psdFile"]) : App.project.basePath;
    csharp_1.FairyEditor.IOUtil.BrowseForOpen("选择要解析的psd文件", psdPath, null, (file) => {
        if (!file.endsWith("psd")) {
            App.Alert("请选择psd文件");
            return;
        }
        file && (view['input_psd'].text = file);
        cacheMap["psdFile"] = file;
        saveCache();
    });
}
function converPsd(view) {
    let psdFilePath = view['input_psd'].text || "";
    let packName = view['input_package'].text || "";
    let comName = view['input_com'].text || "";
    if (!psdFilePath.endsWith("psd")) {
        App.Alert("请选择psd文件");
        return;
    }
    if (!csharp_1.System.IO.File.Exists(psdFilePath)) {
        App.Alert(`不存在改文件${psdFilePath}`);
        return;
    }
    if (!packName) {
        App.Alert("请输入组件的所在的包");
        return;
    }
    if (!comName) {
        App.Alert("请输入组件的名称");
        return;
    }
    startConvertPsd2Fgui(view, psdFilePath, packName, comName);
}
function touchCallback(view, betnName) {
    if (betnName == 'btn_close') {
        closePsd2FguiDialog(view);
    }
    else if (betnName == 'btn_browse') {
        browsePsdFile(view);
    }
    else if (betnName == 'btn_ok') {
        converPsd(view);
    }
}
function createPsd2FguiDialog() {
    let panel = csharp_1.FairyGUI.UIPackage.CreateObject("psd2fgui", "main").asCom;
    let view = new csharp_1.FairyGUI.GComponent();
    view.x = Math.ceil((1980 - panel.viewWidth) * 0.5);
    view.y = Math.ceil((1080 - panel.viewHeight) * 0.5);
    view.AddChild(panel);
    view.draggable = true;
    csharp_1.FairyGUI.GRoot.inst.AddChild(view);
    for (let i = 0; i < panel.numChildren; i++) {
        let disObj = panel.GetChildAt(i);
        view[disObj.name] = disObj;
        if (disObj instanceof csharp_1.FairyGUI.GButton) {
            disObj.onClick.Add((context) => {
                let sender = context.sender;
                let btnName = sender ? sender.name : "";
                touchCallback(view, btnName);
            });
        }
    }
    if (cacheMap["psdFile"]) {
        view['input_psd'].text = cacheMap["psdFile"];
    }
    return view;
}
function closePsd2FguiDialog(view) {
    for (let i = 0; i < view.numChildren; i++) {
        let disObj = view.GetChildAt(i);
        if (disObj instanceof csharp_1.FairyGUI.GButton) {
            disObj.onClick.Clear();
        }
    }
    view.RemoveFromParent();
}
function startConvertPsd2Fgui(view, psdFilePath, packName, comName) {
    let txtMsg = view['txt_msg'];
    txtMsg.text = "转换中...";
    let arr = csharp_1.System.Array.CreateInstance(puerts_1.$typeof(csharp_1.System.String), 4);
    arr.SetValue(psdFilePath, 0);
    let outPath = App.project.assetsPath;
    arr.SetValue(outPath, 1);
    arr.SetValue(packName, 2);
    arr.SetValue(comName, 3);
    let batPath = ROOT_BIN + "psd2fgui.bat";
    console.log("----:", batPath, psdFilePath, outPath, packName, comName);
    setTimeout(() => {
        let exeStr = csharp_1.FairyEditor.ProcessUtil.Start(batPath, arr, ROOT_BIN, true);
        txtMsg.text = "";
        let packname = packName;
        App.Alert(exeStr ? "转换失败" : "转换完成", null, () => {
            App.RefreshProject();
            let fPack = App.project.GetPackageByName(packname);
            let fPackItem = fPack.FindItemByName(comName);
            App.libView.OpenResource(fPackItem);
            App.libView.Highlight(fPackItem, true);
            closePsd2FguiDialog(view);
        });
    }, 20);
}
App.add_onProjectOpened(() => {
    let menu = csharp_1.FairyEditor.App.menu.GetSubMenu("tool");
    menu && menu.AddSeperator();
    menu && menu.AddItem("加载psd", "", () => {
        createPsd2FguiDialog();
    });
});
csharp_1.FairyEditor.App.pluginManager.LoadUIPackage(FAIRUI + 'psd2fgui');
