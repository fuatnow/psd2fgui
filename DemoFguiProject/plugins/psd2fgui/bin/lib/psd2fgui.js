var psd2fgui = (function () {
    'use strict';

    /**
     * 导出参数
     */
    var E_Option;
    (function (E_Option) {
        E_Option[E_Option["DEFAULT"] = 0] = "DEFAULT";
        E_Option[E_Option["IGNORE_FONT"] = 1] = "IGNORE_FONT";
        E_Option[E_Option["NO_PACK"] = 2] = "NO_PACK";
    })(E_Option || (E_Option = {}));
    /**
     * 控件前缀
     */
    var E_Prefix;
    (function (E_Prefix) {
        E_Prefix["COMPONENT"] = "Com$";
        E_Prefix["BUTTON"] = "Btn$";
        E_Prefix["RADIOBUTTON"] = "RadBtn$";
        E_Prefix["CHECKBUTTON"] = "ChkBtn$";
        E_Prefix["PROGRESSBAR"] = "Bar$";
    })(E_Prefix || (E_Prefix = {}));
    const suffixValArr = ['@title', '@icon', '@checked', '@titleColor'];
    /**
     * 控件后缀
     */
    var E_Suffix;
    (function (E_Suffix) {
        E_Suffix["TITLE"] = "@title";
        E_Suffix["ICON"] = "@icon";
        E_Suffix["CHECKED"] = "@checked";
        E_Suffix["BAR"] = "@bar";
        E_Suffix["Grid9"] = "@9#";
    })(E_Suffix || (E_Suffix = {}));
    /**
     * 按钮状态
     */
    var E_ButtonStatus_Suffix;
    (function (E_ButtonStatus_Suffix) {
        E_ButtonStatus_Suffix["Up"] = "@up";
        E_ButtonStatus_Suffix["Down"] = "@down";
        E_ButtonStatus_Suffix["CHECKED"] = "@check";
    })(E_ButtonStatus_Suffix || (E_ButtonStatus_Suffix = {}));
    /**
     * 控件类型
     */
    var E_Item_Type;
    (function (E_Item_Type) {
        E_Item_Type["IMAGE"] = "image";
        E_Item_Type["COMPONENT"] = "component";
    })(E_Item_Type || (E_Item_Type = {}));

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    var __createBinding = Object.create ? (function(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
    }) : (function(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    });

    function __exportStar(m, o) {
        for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(o, p)) __createBinding(o, m, p);
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    /** @deprecated */
    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    /** @deprecated */
    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    }

    function __spreadArray(to, from) {
        for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
            to[j] = from[i];
        return to;
    }

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    var __setModuleDefault = Object.create ? (function(o, v) {
        Object.defineProperty(o, "default", { enumerable: true, value: v });
    }) : function(o, v) {
        o["default"] = v;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
        __setModuleDefault(result, mod);
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    const fs = require('fs');
    const path = require('path');
    const crypto = require('crypto');
    const resizeImg = require('resize-img');
    const PNG = require('pngjs').PNG;
    const { pinyin } = require('pinyin-pro');
    const jpeg = require('jpeg-js');
    const copydir = require('copy-dir');
    class Utils {
        /**
         * 获取文件夹下面的所有的文件(包括子文件夹)
         * @param dirPath
         * @param outArr
         * @param allPath 是否显示文件全路径
         */
        static getAllFiles(dirPath, outArr, allPath = true) {
            fs.readdirSync(dirPath).forEach((name) => {
                const filePath = path.join(dirPath, name);
                const stat = fs.statSync(filePath);
                if (stat.isFile()) {
                    outArr.push(allPath ? filePath : name);
                }
                else if (stat.isDirectory()) {
                    Utils.getAllFiles(filePath, outArr);
                }
            });
        }
        static copyDir(srcPath, desPath) {
            copydir.sync(srcPath, desPath, {
                utimes: true,
                mode: true,
                cover: true // cover file when exists, default is true
            });
        }
        static rmDir(dirPath, cb = null) {
            let files = [];
            // console.log("开始删除")
            if (fs.existsSync(dirPath)) {
                var count = 0;
                var checkEnd = () => {
                    // console.log("进度", count)
                    ++count == files.length && cb && cb();
                };
                files = fs.readdirSync(dirPath);
                files.forEach(function (file) {
                    let curPath = dirPath + "/" + file;
                    if (fs.statSync(curPath).isDirectory()) {
                        // console.log("遇到文件夹", curPath)
                        Utils.rmDir(curPath, checkEnd);
                    }
                    else {
                        fs.unlinkSync(curPath);
                        // console.log("删除文件完成", curPath)
                        checkEnd();
                    }
                });
                // 如果删除文件夹为放置文件夹根目录  不执行删除
                fs.rmdirSync(dirPath);
                //为空时直接回调
                files.length === 0 && cb && cb();
            }
            else {
                cb && cb();
            }
        }
        /**
         * 是否是纹理资源
         * @param fileName
         */
        static isTextureFile(fileName) {
            if (fileName.endsWith(".png") || fileName.endsWith(".jpg")) {
                return true;
            }
            return false;
        }
        /**
         * 根据key生成长度为len的md5码
         * @param key
         * @param len
         */
        static getMd5(key, len = undefined) {
            let hash = crypto.createHash('md5').update(key).digest('hex');
            len = len ? len : hash.length;
            let packId = hash.substr(0, len);
            return packId;
        }
        /**
         * 顔色转换
         * @param rgbaArray
         * @param includingAlpha
         */
        static convertToHtmlColor(rgbaArray, includingAlpha = false) {
            var result = '#';
            var str;
            if (includingAlpha) {
                str = rgbaArray[3].toString(16);
                if (str.length == 1)
                    str = '0' + str;
                result += str;
            }
            for (var i = 0; i < 3; i++) {
                str = rgbaArray[i].toString(16);
                if (str.length == 1)
                    str = '0' + str;
                result += str;
            }
            return result;
        }
        /**
         * 初始化纹理数据
         * @param rowLen
         * @param colLen
         * @param imgBuff
         * @param buffWidth
         * @param offRow
         * @param offCol
         */
        static initNumArr(rowLen, colLen, imgBuff, buffWidth, offRow, offCol) {
            let numArr = [];
            for (let i = offRow; i < rowLen; i++) {
                let baseIdx = i * buffWidth * 4;
                let arr = [];
                numArr.push(arr);
                for (let j = offCol; j < colLen; j++) {
                    let p = baseIdx + j * 4;
                    arr.push(imgBuff[p], imgBuff[p + 1], imgBuff[p + 2], imgBuff[p + 3]);
                }
            }
            return numArr;
        }
        /**
         * 格式化成png数据
         * @param numArr
         */
        static format2PngData(numArr, outArr, idx) {
            return __awaiter(this, void 0, void 0, function* () {
                return new Promise((resolve, reject) => {
                    let height = numArr.length;
                    if (!height) {
                        resolve();
                        return;
                    }
                    let width = numArr[0].length >> 2;
                    let png = new PNG({
                        filterType: 4,
                        width: width,
                        height: height
                    });
                    let rgbaArr = [];
                    for (let i = 0; i < height; i++) {
                        rgbaArr = rgbaArr.concat(numArr[i]);
                    }
                    png.data = new Uint8Array(rgbaArr);
                    let outBuff = [];
                    png.on('data', (data) => {
                        for (let i = 0; i < data.length; i++) {
                            outBuff.push(data[i]);
                        }
                    });
                    png.on('end', () => {
                        let buff = new Uint8Array(outBuff);
                        outArr[idx] = Buffer.from(buff);
                        resolve(buff);
                    });
                    png.pack();
                });
            });
        }
        /**
         * 合并纹理数据
         * @param letfUpArr
         * @param midUpArr
         * @param rightUpArr
         * @param midLeftArr
         * @param midCentertArr
         * @param midRightArr
         * @param leftBottomArr
         * @param midBottomArr
         * @param rightBottomArr
         * @param option
         */
        static mergeData(letfUpArr, midUpArr, rightUpArr, midLeftArr, midCentertArr, midRightArr, leftBottomArr, midBottomArr, rightBottomArr, option) {
            let rgbaArray = [];
            let rgbaArrFun = (arr, row, col) => {
                col = col << 2;
                return [arr[row][col], arr[row][col + 1], arr[row][col + 2], arr[row][col + 3]];
            };
            let isNullArry = (numArr) => {
                return !numArr || numArr.length == 0;
            };
            for (let i = 0, n = option.up + option.bottom + option.middleHeight; i < n; i++) {
                for (let j = 0, m = option.left + option.right + option.middleWidth; j < m; j++) {
                    if (i < option.up) {
                        if (j < option.left) {
                            let row = i, col = j;
                            !isNullArry(letfUpArr) && rgbaArray.push(...rgbaArrFun(letfUpArr, row, col));
                        }
                        else if (j < m - option.left) {
                            let row = i, col = j - option.left;
                            !isNullArry(midUpArr) && rgbaArray.push(...rgbaArrFun(midUpArr, row, col));
                        }
                        else {
                            let row = i, col = j - option.left - option.middleWidth;
                            !isNullArry(rightUpArr) && rgbaArray.push(...rgbaArrFun(rightUpArr, row, col));
                        }
                    }
                    else if (i < n - option.up) {
                        if (j < option.left) {
                            let row = i - option.up, col = j;
                            !isNullArry(midLeftArr) && rgbaArray.push(...rgbaArrFun(midLeftArr, row, col));
                        }
                        else if (j < m - option.left) {
                            let row = i - option.up, col = j - option.left;
                            !isNullArry(midCentertArr) && rgbaArray.push(...rgbaArrFun(midCentertArr, row, col));
                        }
                        else {
                            let row = i - option.up, col = j - option.left - option.middleWidth;
                            !isNullArry(midRightArr) && rgbaArray.push(...rgbaArrFun(midRightArr, row, col));
                        }
                    }
                    else {
                        if (j < option.left) {
                            let row = i - option.up - option.middleHeight, col = j;
                            !isNullArry(leftBottomArr) && rgbaArray.push(...rgbaArrFun(leftBottomArr, row, col));
                        }
                        else if (j < m - option.left) {
                            let row = i - option.up - option.middleHeight, col = j - option.left;
                            !isNullArry(midBottomArr) && rgbaArray.push(...rgbaArrFun(midBottomArr, row, col));
                        }
                        else {
                            let row = i - option.up - option.middleHeight, col = j - option.left - option.middleWidth;
                            !isNullArry(rightBottomArr) && rgbaArray.push(...rgbaArrFun(rightBottomArr, row, col));
                        }
                    }
                }
            }
            return new Uint8Array(rgbaArray);
        }
        /**
         * 设置img大小
         * @param imgData 源img数据
         * @param option 宽高
         */
        static formatScale9Buff(imgBuff, option) {
            return __awaiter(this, void 0, void 0, function* () {
                let letfUpArr = Utils.initNumArr(option.up, option.left, imgBuff, option.width, 0, 0);
                let midUpArr = Utils.initNumArr(option.up, option.width - option.right, imgBuff, option.width, 0, option.left);
                let rightUpArr = Utils.initNumArr(option.up, option.width, imgBuff, option.width, 0, option.width - option.right);
                let midLeftArr = Utils.initNumArr(option.height - option.bottom, option.left, imgBuff, option.width, option.up, 0);
                let midCentertArr = Utils.initNumArr(option.height - option.bottom, option.width - option.right, imgBuff, option.width, option.up, option.left);
                let midRightArr = Utils.initNumArr(option.height - option.bottom, option.width, imgBuff, option.width, option.up, option.width - option.right);
                let leftBottomArr = Utils.initNumArr(option.height, option.left, imgBuff, option.width, option.height - option.up, 0);
                let midBottomArr = Utils.initNumArr(option.height, option.width - option.right, imgBuff, option.width, option.height - option.up, option.left);
                let rightBottomArr = Utils.initNumArr(option.height, option.width, imgBuff, option.width, option.height - option.up, option.width - option.right);
                return new Promise((resolve, _) => {
                    let pngDataArr = [];
                    let savePromises = [
                        Utils.format2PngData(midUpArr, pngDataArr, 0),
                        Utils.format2PngData(midLeftArr, pngDataArr, 1),
                        Utils.format2PngData(midBottomArr, pngDataArr, 2),
                        Utils.format2PngData(midRightArr, pngDataArr, 3),
                        Utils.format2PngData(midCentertArr, pngDataArr, 4),
                    ];
                    let reszie = (numArr, w, h, uint8Buff) => __awaiter(this, void 0, void 0, function* () {
                        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                            if (!uint8Buff) {
                                resolve();
                                return;
                            }
                            let data = yield resizeImg(uint8Buff, { width: w, height: h });
                            new PNG().parse(data, (_, png) => {
                                //把缩放的数据存入到对应的内存区域中
                                let buff = png.data;
                                numArr.length = 0;
                                for (let i = 0; i < png.height; i++) {
                                    let baseIdx = i * png.width * 4;
                                    let arr = [];
                                    numArr.push(arr);
                                    for (let j = 0; j < png.width; j++) {
                                        let p = baseIdx + j * 4;
                                        arr.push(buff[p], buff[p + 1], buff[p + 2], buff[p + 3]);
                                    }
                                }
                                resolve();
                            });
                        }));
                    });
                    let reszieAll = () => __awaiter(this, void 0, void 0, function* () {
                        let resizePromises = [
                            reszie(midUpArr, option.middleWidth, option.up, pngDataArr[0]),
                            reszie(midLeftArr, option.left, option.middleHeight, pngDataArr[1]),
                            reszie(midBottomArr, option.middleWidth, option.bottom, pngDataArr[2]),
                            reszie(midRightArr, option.right, option.middleHeight, pngDataArr[3]),
                            reszie(midCentertArr, option.middleWidth, option.middleHeight, pngDataArr[4]),
                        ];
                        Promise.all(resizePromises).then(() => {
                            let scale9Data = this.mergeData(letfUpArr, midUpArr, rightUpArr, midLeftArr, midCentertArr, midRightArr, leftBottomArr, midBottomArr, rightBottomArr, option);
                            let png = new PNG({
                                filterType: 4,
                                width: option.left + option.middleWidth + option.right,
                                height: option.up + option.middleHeight + option.bottom
                            });
                            png.data = scale9Data;
                            // png.pack().pipe(fs.createWriteStream("test2.png"));
                            resolve(scale9Data);
                        });
                    });
                    Promise.all(savePromises).then(reszieAll);
                });
            });
        }
        static chinese2pinyin(chineseStr) {
            if (!chineseStr)
                return '';
            let res = pinyin(chineseStr, { toneType: 'none', type: 'array' });
            res = res.filter((val) => { return val.trim(); });
            return res ? res.join('_') : '';
        }
        static isChineseStr(str) {
            return escape(str).indexOf('%u') != -1;
        }
        static formatFileName(name) {
            let fileName = name;
            let suffixIdx = fileName.lastIndexOf('.');
            let ext = suffixIdx != -1 ? fileName.substr(suffixIdx) : '';
            if (Utils.isChineseStr(name)) {
                console.warn('文件名称有中文 可能会发生异常: ' + name);
                fileName = Utils.chinese2pinyin(name).replace(' ', '');
            }
            let nameStartIdx = fileName.indexOf('$') + 1;
            for (let i = nameStartIdx, n = fileName.length; i < n; i++) {
                let ch = fileName[i];
                let isLetterNum = (ch == '_') || (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9');
                if (!isLetterNum) {
                    fileName = fileName.substr(nameStartIdx, i - nameStartIdx);
                    break;
                }
            }
            //console.log(name,fileName + ext);
            return fileName + ext;
        }
        static getImgData(filePath) {
            return __awaiter(this, void 0, void 0, function* () {
                return new Promise((resolve, _) => {
                    let fileData = fs.readFileSync(filePath);
                    let data;
                    if (filePath.endsWith('.jpg')) {
                        data = jpeg.decode(fileData, { useTArray: true });
                        resolve(data);
                    }
                    else {
                        new PNG({ filterType: 4 }).parse(fileData, function (error, data) {
                            resolve(data);
                        });
                    }
                });
            });
        }
        static isFileExist(fileName) {
            return fs.existsSync(fileName);
        }
        static getValueByKey(key, str) {
            let pattern = new RegExp(`(${key}(\s*)=(\s*)"([^"]+)")`, "g");
            let valTmpArr = str.match(pattern);
            if (valTmpArr) {
                let val = valTmpArr[0].split('"')[1];
                return val;
            }
            return "";
        }
    }

    class PackageItem {
        constructor(type, fileName, data, id) {
            this._type = type;
            this._id = id;
            this._name = fileName;
            this._data = data;
            this._scale9Grid = undefined;
            this._existed = false;
        }
        get id() {
            return this._id;
        }
        set id(val) {
            this._id = val;
        }
        get type() {
            return this._type;
        }
        get data() {
            return this._data;
        }
        get width() {
            return this._data.get('width');
        }
        get height() {
            return this._data.get('height');
        }
        get name() {
            return this._name;
        }
        get existed() {
            return this._existed;
        }
        set existed(val) {
            this._existed = val;
        }
        /**
         * 格式化九宫格数据
         */
        format9ScaleData() {
            return __awaiter(this, void 0, void 0, function* () {
                if (!this._scale9Grid)
                    return;
                let channelImg = this.data.get('image');
                let imgBuff = Buffer.from(channelImg.pixelData);
                let scale9Width = this._scale9Grid[0] + this._scale9Grid[4] + this._scale9Grid[2];
                let scale9Height = this._scale9Grid[1] + this._scale9Grid[3] + this._scale9Grid[5];
                let newImgUint8 = yield Utils.formatScale9Buff(imgBuff, {
                    width: this.width,
                    height: this.height,
                    left: Number(this._scale9Grid[0]),
                    up: Number(this._scale9Grid[1]),
                    middleWidth: Number(this._scale9Grid[2]),
                    middleHeight: Number(this._scale9Grid[3]),
                    right: Number(this._scale9Grid[4]),
                    bottom: Number(this._scale9Grid[5])
                });
                channelImg.obj._width = scale9Width;
                channelImg.obj._height = scale9Height;
                channelImg.obj.pixelData = newImgUint8;
            });
        }
        /**
         * 设置9宫左上右下
         */
        set scale9Grid(scale9Grid) {
            if (scale9Grid && scale9Grid.length > 0) {
                this._scale9Grid = scale9Grid;
            }
        }
        get scale9Grid() {
            return this._scale9Grid;
        }
    }

    const fs$1 = require('fs-extra');
    const path$1 = require('path');
    const psd = require('psd');
    const xmlbuilder = require('xmlbuilder');
    const jpeg$1 = require('jpeg-js');
    class UIPackage {
        constructor(cfg) {
            this._tmpDir = 'tmp'; //临时路径
            this._buildId = Utils.getMd5(`${cfg.mainCom}${cfg.subCom}`, 8);
            this._resBuildId = Utils.getMd5(`${cfg.mainCom}_Res`.toLowerCase(), 8);
            this._nextItemIndex = 0;
            this._sameDataTestHelper = {};
            this._sameNameTestHelper = {};
            this._texture_Reses = [];
            this._component_Reses = [];
            this._cfg = cfg;
            this._comExportPath = path$1.join(this._cfg.outPath, this._cfg.mainCom, this._cfg.subCom);
            this._resExportPath = path$1.join(this._cfg.outPath, this._cfg.mainCom + '_Res');
            this._comExportPathTmp = path$1.join(this._tmpDir, this._cfg.mainCom, this._cfg.subCom);
            this._resExportPathTmp = path$1.join(this._tmpDir, this._cfg.mainCom + '_Res');
            fs$1.ensureDirSync(this._comExportPath);
            fs$1.ensureDirSync(this._resExportPath);
            Utils.rmDir(this._tmpDir);
            fs$1.ensureDirSync(this._comExportPathTmp);
            fs$1.ensureDirSync(this._resExportPathTmp);
        }
        get buildId() {
            return this._buildId;
        }
        get cfg() {
            return this._cfg;
        }
        loadResPackage() {
            return __awaiter(this, void 0, void 0, function* () {
                let resXmlPath = path$1.join(this._resExportPath, "package.xml");
                let isResPackExist = Utils.isFileExist(resXmlPath);
                if (isResPackExist) {
                    let xmlData = fs$1.readFileSync(path$1.join(this._resExportPath, 'package.xml'), 'utf-8');
                    let idNameArrs = xmlData.match(/<image(.*)>/g);
                    let cnt = idNameArrs ? idNameArrs.length : 0;
                    for (let i = 0; i < cnt; i++) {
                        let str = idNameArrs[i];
                        if (str) {
                            let id = Utils.getValueByKey('id', str);
                            let fileName = Utils.getValueByKey('name', str);
                            let scale9Grid = Utils.getValueByKey('scale9grid', str);
                            let dataForHash = null;
                            let packItem = new PackageItem(E_Item_Type.IMAGE, fileName, null, id);
                            packItem.existed = true;
                            let imgData = yield Utils.getImgData(path$1.join(this._resExportPath, fileName));
                            if (fileName.indexOf('.jpg') != -1) {
                                // console.log('imgData:',imgData);
                                dataForHash = Buffer.from(imgData.data);
                            }
                            else {
                                if (scale9Grid) {
                                    let scale9Arr = scale9Grid.split(',').map(Number);
                                    scale9Arr[4] = imgData.width - Number(scale9Arr[0]) - Number(scale9Arr[2]);
                                    scale9Arr[5] = imgData.height - Number(scale9Arr[1]) - Number(scale9Arr[3]);
                                    packItem.scale9Grid = scale9Arr;
                                    let lastIdx = fileName.lastIndexOf('.');
                                    let ext = fileName.substr(lastIdx);
                                    let name = fileName.substr(0, lastIdx);
                                    let dec = scale9Arr.join('_');
                                    dataForHash = `${name}${E_Suffix.Grid9}${dec}${ext}`;
                                }
                                else {
                                    dataForHash = Buffer.from(imgData.data);
                                }
                            }
                            let hash = Utils.getMd5(dataForHash);
                            this._sameDataTestHelper[hash] = packItem;
                            this._texture_Reses.push(packItem);
                        }
                    }
                }
            });
        }
        /**
         * 导出fgui包 [资源包+组件包]
         */
        exportPackge() {
            return __awaiter(this, void 0, void 0, function* () {
                yield this.loadResPackage();
                let psdPraser = psd.fromFile(this._cfg.psdFile);
                psdPraser.parse();
                let psdTree = psdPraser.tree();
                //console.log(JSON.stringify(psdTree.export()));
                let subComPackItem = this.createComponent(psdTree, this._cfg.subCom);
                subComPackItem.id = this._buildId;
                return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                    let exportResOk = yield this.exportResPackage();
                    if (exportResOk) {
                        console.log('res export ok->' + this._resExportPathTmp);
                        let exportComOk = yield this.exprotComPackage();
                        if (exportComOk) {
                            console.log('com export ok->' + this._comExportPathTmp);
                            console.log(`copy ${this._tmpDir} to ${this._cfg.outPath}`);
                            Utils.copyDir(this._tmpDir, this._cfg.outPath);
                            console.log(`rmdir ${this._tmpDir}`);
                            Utils.rmDir(this._tmpDir);
                            console.log(`export finish!`);
                            resolve(this);
                        }
                        else {
                            reject(null);
                        }
                    }
                    else {
                        reject(null);
                    }
                }));
            });
        }
        /**
         * 导出资源包
         */
        exportResPackage() {
            return __awaiter(this, void 0, void 0, function* () {
                if (this._comExportPath == this._resExportPath) {
                    console.error('res buildId same with com build');
                    return false;
                }
                let saveImg = (item) => __awaiter(this, void 0, void 0, function* () {
                    let outPath = path$1.join(this._resExportPathTmp, item.name);
                    if (outPath.endsWith('jpg')) {
                        return new Promise((resolve, reject) => {
                            fs$1.writeFileSync(outPath, item.data.data);
                            resolve();
                        });
                    }
                    else if (item.scale9Grid) {
                        yield item.format9ScaleData();
                    }
                    let pngPims = item.data.saveAsPng(outPath);
                    return pngPims;
                });
                return new Promise((resolve, reject) => {
                    let savePromises = [];
                    let pkgDesc = xmlbuilder.create('packageDescription');
                    pkgDesc.att('id', this._resBuildId);
                    let resourcesNode = pkgDesc.ele('resources');
                    this._texture_Reses.forEach((item) => {
                        let resNode = resourcesNode.ele(E_Item_Type.IMAGE);
                        resNode.att('id', item.id)
                            .att('name', item.name)
                            .att('path', `/`)
                            .att('exported', true);
                        if (item.scale9Grid) {
                            resNode.att('scale', '9grid');
                            resNode.att('scale9grid', item.scale9Grid);
                        }
                        if (!item.existed) {
                            savePromises.push(saveImg(item));
                        }
                    });
                    savePromises.push(fs$1.writeFile(path$1.join(this._resExportPathTmp, 'package.xml'), pkgDesc.end({ pretty: true })));
                    Promise.all(savePromises).then(() => {
                        resolve(true);
                    }).catch(_ => reject(false));
                });
            });
        }
        /**
         * 导出组件包
         */
        exprotComPackage() {
            return __awaiter(this, void 0, void 0, function* () {
                let genNewFileData = (packagePath, fileData) => {
                    let newFileData = fileData;
                    let newComponets = newFileData.match(/<component(.*)>/g);
                    if (newComponets && newComponets.length && Utils.isFileExist(packagePath)) {
                        let packData = fs$1.readFileSync(packagePath, 'utf-8');
                        let reg = /<resources>([\s\S]+)<\/resources>/g;
                        let resources = packData.match(reg);
                        if (resources && resources.length) {
                            let componets = resources[0].match(/<(.*)\/>/g);
                            if (componets && componets.length > 0) {
                                for (let i = 0, cnt = newComponets.length; i < cnt; i++) {
                                    let newComponet = newComponets[i];
                                    const newName = Utils.getValueByKey('name', newComponet);
                                    const newPath = Utils.getValueByKey('path', newComponet);
                                    let findCompont = componets.find((val, _idx, comps) => {
                                        const name = Utils.getValueByKey('name', val);
                                        const path = Utils.getValueByKey('path', val);
                                        if (name == newName && path == newPath) {
                                            comps[_idx] = newComponet;
                                            return val;
                                        }
                                    });
                                    if (!findCompont) {
                                        componets.push(newComponet);
                                    }
                                }
                                let componentStr = `<resources>\n\t` + componets.join('\n\t') + `\n  </resources>`;
                                newFileData = packData.replace(reg, componentStr);
                            }
                        }
                    }
                    return newFileData;
                };
                return new Promise((resolve, reject) => {
                    let pkgDesc = xmlbuilder.create('packageDescription');
                    pkgDesc.att('id', this._buildId);
                    let resourcesNode = pkgDesc.ele('resources');
                    let savePromises = [];
                    this._component_Reses.forEach((item) => {
                        let resNode = resourcesNode.ele(item.type);
                        resNode.att('id', item.id).att('name', item.name).att('path', `/${this._cfg.subCom}/`);
                        savePromises.push(fs$1.writeFile(path$1.join(this._comExportPathTmp, item.name), item.data));
                    });
                    let filePathTmp = path$1.join(this._tmpDir, this._cfg.mainCom, 'package.xml');
                    let desFilePath = path$1.join(this._cfg.outPath, this._cfg.mainCom, 'package.xml');
                    let packageFileData = genNewFileData(desFilePath, pkgDesc.end({ pretty: true }));
                    savePromises.push(fs$1.writeFile(filePathTmp, packageFileData));
                    Promise.all(savePromises).then(() => {
                        resolve(true);
                    }).catch(_ => reject(false));
                });
            });
        }
        /**
         * 组件id
         */
        getNextItemId() {
            return this._buildId + (this._nextItemIndex++).toString(36);
        }
        /**
         * 创建组件
         * @param aNode
         * @param name
         */
        createComponent(aNode, name = '') {
            let component = xmlbuilder.create('component');
            component.att('size', aNode.get('width') + ',' + aNode.get('height'));
            let displayList = component.ele('displayList');
            let cnt = aNode.children().length;
            for (let i = cnt - 1; i >= 0; i--) {
                this.parseNode(aNode.children()[i], aNode, displayList);
            }
            return this.createPackageItem(E_Item_Type.COMPONENT, (name ? name : aNode.get('name')) + '.xml', component.end({ pretty: true }));
        }
        /**
         * 创建进度条
         * @param aNode
         */
        createProgressBar(aNode) {
            let component = xmlbuilder.create('component');
            component.att('size', aNode.get('width') + ',' + aNode.get('height'));
            component.att('extention', 'ProgressBar');
            let onElementCallback = (child, node) => {
                let nodeName = node.get('name');
                if (nodeName.indexOf(E_Suffix.BAR) != -1) {
                    child.att('name', 'bar');
                }
            };
            let displayList = component.ele('displayList');
            for (let i = aNode.children().length - 1; i >= 0; i--) {
                this.parseNode(aNode.children()[i], aNode, displayList, onElementCallback);
            }
            return this.createPackageItem(E_Item_Type.COMPONENT, aNode.get('name') + '.xml', component.end({ pretty: true }));
        }
        /**
         * 创建按钮组件
         * @param aNode
         * @param instProps 显示属性
         */
        createButton(aNode, instProps) {
            let component = xmlbuilder.create('component');
            component.att('size', aNode.get('width') + ',' + aNode.get('height'));
            component.att('extention', 'Button');
            let btnStatuesImgs = [];
            let imagePages = [];
            aNode.descendants().forEach((childNode) => {
                let nodeName = childNode.get('name');
                let idx = 0;
                for (let i in E_ButtonStatus_Suffix) {
                    let key = i;
                    if (nodeName.indexOf(E_ButtonStatus_Suffix[key]) != -1) {
                        btnStatuesImgs[idx] = childNode;
                    }
                    idx++;
                }
                ;
            });
            if (btnStatuesImgs.length > 1) {
                imagePages[0] = [0, 2];
                imagePages[1] = [1, 3];
                imagePages[2] = [1, 3];
            }
            else {
                imagePages[0] = [0, 1, 2, 3];
            }
            let onElementCallback = (child, node) => {
                let j = btnStatuesImgs.indexOf(node);
                if (j != -1) {
                    let gear = child.ele('gearDisplay');
                    gear.att('controller', 'button');
                    gear.att('pages', imagePages[j].join(','));
                }
                if (child.attributes['text']) {
                    child.att('name', 'title');
                    instProps[E_Suffix.TITLE] = child.attributes['text'].value;
                    instProps['@titleColor'] = child.attributes['color'].value;
                    // child.removeAttribute('text');
                }
                else if (child.attributes['url']) {
                    instProps[E_Suffix.ICON] = child.attributes['url'].value;
                    // child.removeAttribute('url');
                }
            };
            let controller = component.ele('controller');
            controller.att('name', 'button');
            controller.att('pages', '0,up,1,down,2,over,3,selectedOver');
            let displayList = component.ele('displayList');
            for (let i = aNode.children().length - 1; i >= 0; i--) {
                let child = aNode.children()[i];
                this.parseNode(child, aNode, displayList, onElementCallback);
            }
            let extension = component.ele('Button');
            if (aNode.get('name').indexOf(E_Prefix.CHECKBUTTON) == 0) {
                extension.att('mode', 'Check');
                instProps[E_Suffix.CHECKED] = 'true';
            }
            else if (aNode.get('name').indexOf(E_Prefix.RADIOBUTTON) == 0) {
                extension.att('mode', 'Radio');
            }
            if (btnStatuesImgs.length <= 1) {
                extension.att('downEffect', 'scale');
                extension.att('downEffectValue', '1.1');
            }
            return this.createPackageItem(E_Item_Type.COMPONENT, aNode.get('name') + '.xml', component.end({ pretty: true }));
        }
        /**
         * 创建img
         * @param aNode
         * @param scale9Grid
         */
        createImage(aNode) {
            let width = aNode.get('width');
            let height = aNode.get('height');
            //超大图片保存为jpg
            let imgSuffix = (width > 1024 || height > 1024) ? `_${this._cfg.subCom}.jpg` : '.png';
            let nodeName = aNode.get('name');
            let fFileName = Utils.formatFileName(nodeName);
            let scale9Grid;
            if (nodeName.indexOf(E_Suffix.Grid9) != -1) {
                let pattern = new RegExp(`${E_Suffix.Grid9}([0-9|_]+)`, "g");
                let s9InfoArr = nodeName.match(pattern);
                let s9Info = s9InfoArr ? s9InfoArr[0].substr(E_Suffix.Grid9.length) : '';
                let s9Cfg = s9Info.split('_');
                if (s9Cfg.length >= 1) {
                    let left = Number(s9Cfg[0]);
                    if (left < width && left < height) {
                        let up = Number(s9Cfg[1] || left);
                        let middleWidth = Number(s9Cfg[2] || 4);
                        let middleHeight = Number(s9Cfg[3] || middleWidth);
                        let right = Number(s9Cfg[4] || left);
                        let bottom = Number(s9Cfg[5] || up);
                        scale9Grid = [left, up, middleWidth, middleHeight, right, bottom];
                        let dec = scale9Grid.join('_');
                        fFileName = fFileName + `${E_Suffix.Grid9}${dec}`;
                    }
                }
            }
            let fileName = `${fFileName}${imgSuffix}`;
            let packageItem = this.createPackageItem(E_Item_Type.IMAGE, fileName, aNode);
            packageItem.scale9Grid = scale9Grid;
            return packageItem;
        }
        isButton(nodeName) {
            if (!nodeName)
                return false;
            return nodeName.indexOf(E_Prefix.BUTTON) == 0 || nodeName.indexOf(E_Prefix.CHECKBUTTON) == 0 || nodeName.indexOf(E_Prefix.RADIOBUTTON) == 0;
        }
        /**
         * 创建组件xml
         */
        createXmlComponent(aNode, rootNode, displayList, packageItem) {
            let child = xmlbuilder.create('component');
            let str = 'n' + (displayList.children.length + 1);
            child.att('id', str + '_' + this._buildId);
            child.att('name', str);
            child.att('src', packageItem.id);
            child.att('fileName', packageItem.name);
            child.att('xy', (aNode.left - rootNode.left) + ',' + (aNode.top - rootNode.top));
            return child;
        }
        /**
         * 创建进度条xml
         */
        createXmlProgressBar(aNode, rootNode, displayList, packageItem, specialUsage) {
            let child = xmlbuilder.create('component');
            let str = 'n' + (displayList.children.length + 1);
            child.att('id', str + '_' + this._buildId);
            child.att('name', specialUsage ? specialUsage : str);
            child.att('src', packageItem.id);
            child.att('fileName', packageItem.name);
            child.att('xy', (aNode.left - rootNode.left) + ',' + (aNode.top - rootNode.top));
            child.ele({ ProgressBar: { '@value': 50, '@max': 100 } });
            return child;
        }
        /**
         * 创建按钮xml
         */
        createXmlButton(aNode, rootNode, displayList, packageItem, instProps) {
            let child = xmlbuilder.create('component');
            let str = 'n' + (displayList.children.length + 1);
            child.att('id', str + '_' + this._buildId);
            child.att('name', str);
            child.att('src', packageItem.id);
            child.att('fileName', packageItem.name);
            child.att('xy', (aNode.left - rootNode.left) + ',' + (aNode.top - rootNode.top));
            child.ele({ Button: instProps });
            return child;
        }
        /**
         * 创建文本xml
         */
        createXmlText(aNode, rootNode, displayList, typeTool, specialUsage) {
            let child = xmlbuilder.create('text');
            let str = 'n' + (displayList.children.length + 1);
            child.att('id', str + '_' + this._buildId);
            child.att('name', specialUsage ? specialUsage : str);
            child.att('text', typeTool.textValue);
            if (specialUsage == 'title') {
                child.att('xy', (aNode.left - rootNode.left - 4) + ',' + (aNode.top - rootNode.top - 4));
                child.att('size', rootNode.width + ',' + (aNode.height + 8));
                child.att('align', 'center').att('vAlign', 'middle');
                let relation = xmlbuilder.create('relation');
                relation.att('target', '').att('sidePair', 'center-center,middle-middle');
                child.importDocument(relation);
            }
            else {
                child.att('xy', (aNode.left - rootNode.left - 4) + ',' + (aNode.top - rootNode.top - 4));
                child.att('size', (aNode.width + 8) + ',' + (aNode.height + 8));
                str = typeTool.alignment()[0];
                if (str != 'left') {
                    child.att('align', str);
                }
            }
            child.att('vAlign', 'middle');
            // child.att('autoSize', 'none');
            if (!(this._cfg.option & E_Option.IGNORE_FONT)) {
                child.att('font', typeTool.fonts()[0]);
            }
            child.att('fontSize', typeTool.sizes()[0]);
            child.att('color', Utils.convertToHtmlColor(typeTool.colors()[0]));
            return child;
        }
        /**
         * 创建图片xml
         */
        createXmlImage(aNode, rootNode, displayList, packageItem, specialUsage) {
            let typeName = (specialUsage == 'icon' ? 'loader' : 'image');
            let child = xmlbuilder.create(typeName);
            let str = 'n' + (displayList.children.length + 1);
            child.att('id', str + '_' + this._buildId);
            let name = str;
            if (specialUsage) {
                name = this.isButton(rootNode.get('name')) ? specialUsage : name + "_icon";
            }
            child.att('name', name);
            child.att('xy', (aNode.left - rootNode.left) + ',' + (aNode.top - rootNode.top));
            let sidePair = '';
            if (specialUsage == 'icon') {
                child.att('size', aNode.width + ',' + aNode.height);
                child.att('url', 'ui://' + this._resBuildId + packageItem.id);
                child.att('fill', 'scaleFree').att('align', 'center')
                    .att('vAlign', 'middle').att('autoSize', 'true')
                    .att('pivot', '0.5,0.5');
                sidePair = 'center-center,middle-middle';
            }
            else {
                child.att('src', packageItem.id);
                child.att('pkg', this._resBuildId);
                sidePair = 'width-width,height-height';
                child.att('fileName', packageItem.name);
            }
            //9宫居中放大缩小
            if (packageItem.scale9Grid) {
                child.att('size', aNode.width + ',' + aNode.height);
                let relation = xmlbuilder.create('relation');
                relation.att('target', '').att('sidePair', sidePair);
                child.importDocument(relation);
            }
            return child;
        }
        parseNode(aNode, rootNode, displayList, onElementCallback = null) {
            if (!aNode.visible())
                return;
            let nodeName = aNode.get('name');
            let specialUsage = '', child = null;
            if (nodeName.indexOf(E_Suffix.TITLE) != -1) {
                specialUsage = 'title';
            }
            else if (nodeName.indexOf(E_Suffix.ICON) != -1) {
                specialUsage = 'icon';
            }
            if (aNode.isGroup()) {
                if (nodeName.indexOf(E_Prefix.COMPONENT) == 0) {
                    let packageItem = this.createComponent(aNode);
                    child = this.createXmlComponent(aNode, rootNode, displayList, packageItem);
                }
                else if (nodeName.indexOf(E_Prefix.PROGRESSBAR) == 0) {
                    let packageItem = this.createProgressBar(aNode);
                    child = this.createXmlProgressBar(aNode, rootNode, displayList, packageItem, specialUsage);
                }
                else if (this.isButton(nodeName)) {
                    let instProps = {};
                    let packageItem = this.createButton(aNode, instProps);
                    child = this.createXmlButton(aNode, rootNode, displayList, packageItem, instProps);
                }
                else {
                    for (let i = aNode.children().length - 1; i >= 0; i--) {
                        this.parseNode(aNode.children()[i], rootNode, displayList, onElementCallback);
                    }
                }
            }
            else {
                let typeTool = aNode.get('typeTool');
                if (typeTool) {
                    child = this.createXmlText(aNode, rootNode, displayList, typeTool, specialUsage);
                }
                else if (!aNode.isEmpty()) {
                    let packageItem = this.createImage(aNode);
                    child = this.createXmlImage(aNode, rootNode, displayList, packageItem, specialUsage);
                }
            }
            if (child) {
                let opacity = aNode.get('opacity');
                if (opacity < 255) {
                    child.att('alpha', (opacity / 255).toFixed(2));
                }
                onElementCallback && onElementCallback(child, aNode);
                displayList.importDocument(child);
            }
            return child;
        }
        /**
         * 创建包
         * @param type 类型
         * @param fileName 文件名
         * @param data
         */
        createPackageItem(type, fileName, data) {
            let dataForHash = data, resources = this._component_Reses;
            if (type == E_Item_Type.IMAGE) {
                dataForHash = Buffer.from(data.get('image').pixelData);
                if (fileName.indexOf('.jpg') != -1) {
                    let rawImageData = {
                        data: dataForHash,
                        width: data.width,
                        height: data.height,
                    };
                    data = jpeg$1.encode(rawImageData);
                    let pixelData = jpeg$1.decode(data.data, { useTArray: true });
                    // console.log('pixelData',pixelData);
                    dataForHash = Buffer.from(pixelData.data);
                }
                else if (fileName.indexOf(E_Suffix.Grid9) != -1) {
                    //九宫格直接使用名称做hash
                    dataForHash = fileName;
                }
                resources = this._texture_Reses;
            }
            else if (type == E_Item_Type.COMPONENT) {
                dataForHash = dataForHash.replace(/(\s{1,1})url="(.*?)"/g, ''); //默认url
                dataForHash = dataForHash.replace(/(\s{1,1})text="(.*?)"/g, ''); //默认文本
                dataForHash = dataForHash.replace(/(\s{1,1})xy="(.*?)"/g, ''); //默认位置
                dataForHash = dataForHash.replace(/(\s{1,1})size="(.*?)"/g, ''); //默认size
                dataForHash = dataForHash.replace(/(\s{1,1})color="(.*?)"/g, ''); //color
                dataForHash = dataForHash.replace(/(\s{1,1})src="(.*?)"/g, ''); //默认图片
                dataForHash = dataForHash.replace(/(\s{1,1})fileName="(.*?)"/g, ''); //默认图片
                dataForHash += fileName;
            }
            let id = this.getNextItemId();
            let hash = Utils.getMd5(dataForHash);
            let item = this._sameDataTestHelper[hash];
            if (!item) {
                fileName = Utils.formatFileName(fileName.replace('-', '_'));
                let i = fileName.lastIndexOf('.');
                let basename = fileName.substr(0, i);
                //basename = basename.replace(/[\@\'\'\\\/\b\f\n\r\t\$\%\*\:\?\<\>\|]/g, '_');
                let ext = fileName.substr(i);
                while (true) {
                    let j = this._sameNameTestHelper[basename];
                    if (j) {
                        this._sameNameTestHelper[basename] = j + 1;
                        basename = basename + '_' + j;
                    }
                    else {
                        this._sameNameTestHelper[basename] = 1;
                        break;
                    }
                }
                fileName = basename + ext;
                item = new PackageItem(type, fileName, data, id);
                resources.push(item);
                this._sameDataTestHelper[hash] = item;
            }
            return item;
        }
    }

    class Psd2Fgui {
        static convert(argv) {
            //console.log(argv)
            if (argv.length < 4) {
                console.info('Usage: psd2fgui psdFile outputFile packName subCom. exp: node Psd2Fgui  D:/Project/_psd2fgui/test/test.psd D:/Project/fguiTest/FGUIProject/assets/ Bag Card');
                process.exit(1);
            }
            let option = E_Option.DEFAULT;
            for (var i = 0; i < argv.length; i++) {
                var arg = argv[i];
                if (arg.indexOf('--') == 0) {
                    switch (arg.substr(2)) {
                        case 'ignore-font':
                            option |= E_Option.IGNORE_FONT;
                            break;
                        default:
                            console.error('unknown argument: ' + arg);
                            break;
                    }
                }
            }
            let cfg = {
                psdFile: argv[0],
                outPath: argv[1],
                mainCom: argv[2],
                subCom: argv[3],
                option: option,
            };
            new UIPackage(cfg).exportPackge().then((pack) => {
                console.log(`Package (${pack.cfg.mainCom})[${pack.cfg.subCom}] export ok. buildId: ${pack.buildId}`);
            });
        }
    }
    Psd2Fgui.convert(process.argv.slice(2));

    return Psd2Fgui;

}());
//# sourceMappingURL=psd2fgui.js.map
